import java.util.Arrays;

public class rompe3 implements rompecocos {

    private rompe3 origen;
    private long g;
    private int[][] matriz;
    private int origenC;
    private int origenR;
    private manhattan3 resultado;

    public rompe3(int[][] element, rompe3 origen, long g, int renglon, int columna, manhattan3 resultado) {
        this.origen = origen;
        this.g = g;
        this.origenR = renglon;
        this.origenC = columna;
        this.matriz = element;
        if (origenC != -1 && origenR != -1 && element[origenR][origenC] != 0) {
            buscarCero();
        }
        this.resultado = resultado;
    }


    @Override
    public long calculo() {
        return this.g + resultado.getManhattanTres(matriz);
    }

    @Override
    public void buscarCero() {
        for (int renglon = 0; renglon < matriz.length; renglon++) {
            for (int columna = 0; columna < matriz.length; columna++) {
                if (matriz[renglon][columna] == 0) {
                    puntoPivote(renglon, columna);
                }
            }
        }
    }

    @Override
    public rompe3 getOrigen() {
        return origen;
    }

    @Override
    public void puntoPivote(int renglon, int columna) {
        this.origenR = renglon;
        this.origenC = columna;
    }

    @Override
    public int getOrigenC() {
        return origenC;
    }

    @Override
    public int getOrigenR() {
        return origenR;
    }

    @Override
    public void setMatriz(int[][] matriz) {
        this.matriz = matriz;
    }

    @Override
    public void imprimir() {
        for (int renglon = 0; renglon < matriz.length; renglon++) {
            for (int columna = 0; columna < matriz.length; columna++) {
                System.out.print(matriz[renglon][columna] + "  ");
            }
            System.out.println("");
        }
        System.out.println("");
    }

    @Override
    public int[][] getMatriz() {
        return matriz;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final rompe3 other = (rompe3) obj;
        if (this.origenC != other.origenC) {
            return false;
        }
        if (this.origenR != other.origenR) {
            return false;
        }
        if (!Arrays.deepEquals(this.matriz, other.matriz)) {
            return false;
        }
        return true;
    }

    @Override
    public rompe3 clone() {
        int[][] matrizC = new int[3][3];
        matrizC[0][0] = matriz[0][0];
        matrizC[0][1] = matriz[0][1];
        matrizC[0][2] = matriz[0][2];

        matrizC[1][0] = matriz[1][0];
        matrizC[1][1] = matriz[1][1];
        matrizC[1][2] = matriz[1][2];

        matrizC[2][0] = matriz[2][0];
        matrizC[2][1] = matriz[2][1];
        matrizC[2][2] = matriz[2][2];

        rompe3 clone = new rompe3(matrizC, this, this.g + 1, getOrigenR(), getOrigenC(), resultado);
        return clone;
    }

    @Override
    public boolean finalizar() {
        if (matriz[0][0] != 1) {
            return false;
        }
        if (matriz[0][1] != 2) {
            return false;
        }
        if (matriz[0][2] != 3) {
            return false;
        }
        if (matriz[1][0] != 4) {
            return false;
        }
        if (matriz[1][1] != 5) {
            return false;
        }
        if (matriz[1][2] != 6) {
            return false;
        }
        if (matriz[2][0] != 7) {
            return false;
        }
        if (matriz[2][1] != 8) {
            return false;
        }
        return true;
    }
}
