
import java.util.*;
    

/**
 * Autores: Juan Rendon, Juan Sánchez, Guillermo Ceballos
 */

public class main {

    public static Queue<rompecocos> lista;
    public static Set<rompecocos> jVisitados = new HashSet<>();
    public static rompecocos resultado;
    public static manhattan4 cuadro15;
    public static manhattan3 cuadro8;

    public static void main(String[] args) {
        rompecocos inicio;

        Scanner input = new Scanner(System.in);
        System.out.print("Dimension total:");
        int a = input.nextInt();
        if (a == 3) {
            System.out.println("");
            cuadro8 = new manhattan3();
            int[][] estado = new int[3][3];
            System.out.println("Estado inicial: ");
            for (int linea = 0; linea < estado.length; linea++) {
                for (int columna = 0; columna < estado.length; columna++) {
                    System.out.printf("Estado inicial[%s][%s]:  ", linea, columna);
                    estado[linea][columna] = input.nextInt();
                }
            }
            lista = new PriorityQueue<>(100, getNodoComparatorFH2());
            inicio = new rompe3(estado, new rompe3(new int[][]{{0, 0, 0}, {0, 0, 0}, {0, 0, 0}}, null, 0, 0, 0, cuadro8), 0, 0, 0, cuadro8);
            inicio.imprimir();
            lista.add(inicio);
        }
        if (a == 4) {
            System.out.println("");
            cuadro15 = new manhattan4();
            int[][] estado = new int[4][4];
            for (int linea = 0; linea < estado.length; linea++) {
                for (int columna = 0; columna < estado.length; columna++) {
                    System.out.printf("Estado inicial[%s][%s]:  ", linea, columna);
                    estado[linea][columna] = input.nextInt();
                }
            }
            lista = new PriorityQueue<>(100, getNodoComparatorFH2());
            inicio = new rompe4(estado, new rompe4(new int[][]{{0, 0, 0}, {0, 0, 0}, {0, 0, 0}}, null, 0, 0, 0, cuadro15), 0, 0, 0, cuadro15);
            inicio.imprimir();
            lista.add(inicio);
        }

        
        long init = Calendar.getInstance().getTimeInMillis();
        while (resultado == null) {
            if (crearHijos()) {
                break;
            }
        }

        rompecocos n = resultado;
        System.out.println("Solucion: ");
        List<rompecocos> resultadoList = new ArrayList<>();
        while (n != null) {
            resultadoList.add(n);
            n = n.getOrigen();
        }

        for (int i = resultadoList.size() - 3; i > -1; i--) {
            if (i == 0) {
                System.out.printf("Solucion:");
            } else {
                System.out.printf("Iteracion %s", resultadoList.size() - (i + 2));
            }
            System.out.println("");
            resultadoList.get(i).imprimir();
        }

        System.out.println("Tiempo Gastado: " + (Calendar.getInstance().getTimeInMillis() - init) + " ms");
    }

    public static boolean crearHijos() {
        rompecocos actual = lista.remove();
        jVisitados.add(actual);
        int linea = actual.getOrigenR();
        int columna = actual.getOrigenC();
        if (columna != 0) {
            rompecocos t = actual.clone();
            int elemt = t.getMatriz()[linea][columna - 1];
            t.getMatriz()[linea][columna - 1] = 0;
            t.getMatriz()[linea][columna] = elemt;
            t.puntoPivote(linea, columna - 1);
            if (!jVisitados.contains(t)) {
                lista.add(t);
                if (t.finalizar()) {
                    resultado = t;
                    return true;
                }
            }
        }

        if (columna != actual.getMatriz().length - 1) {
            rompecocos t = actual.clone();
            int elemt = t.getMatriz()[linea][columna + 1];
            t.getMatriz()[linea][columna + 1] = 0;
            t.getMatriz()[linea][columna] = elemt;
            t.puntoPivote(linea, columna + 1);

            if (!jVisitados.contains(t)) {
                lista.add(t);
                if (t.finalizar()) {
                    resultado = t;
                    return true;
                }
            }
        }

        if (linea != 0) {
            rompecocos t = actual.clone();
            int elemt = t.getMatriz()[linea - 1][columna];
            t.getMatriz()[linea - 1][columna] = 0;
            t.getMatriz()[linea][columna] = elemt;
            t.puntoPivote(linea - 1, columna);

            if (!jVisitados.contains(t)) {
                lista.add(t);
                if (t.finalizar()) {
                    resultado = t;
                    return true;
                }
            }
        }

        if (linea != actual.getMatriz().length - 1) {
            rompecocos t = actual.clone();
            int[][] mt = t.getMatriz();
            int elemt = mt[linea + 1][columna];
            mt[linea + 1][columna] = 0;
            mt[linea][columna] = elemt;
            t.setMatriz(mt);
            t.puntoPivote(linea + 1, columna);

            if (!jVisitados.contains(t)) {
                lista.add(t);
                if (t.finalizar()) {
                    resultado = t;
                    return true;
                }
            }
        }
        return false;
    }

    static protected Comparator<rompecocos> getNodoComparatorFH2() {
        return (rompecocos no1, rompecocos no2) -> {
            long f1 = no1.calculo();
            long f2 = no2.calculo();
            if (f1 > f2) {
                return 1;
            } else if (f1 == f2) {
                return 0;
            } else {
                return -1;
            }
        };
    }

}
