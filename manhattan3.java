public class manhattan3 {
    public int getManhattanTres(int[][] emAnalise) {
        int aux = 0;
        for (int linha = 0; linha < emAnalise.length; linha++) {
            for (int coluna = 0; coluna < emAnalise.length; coluna++) {
                switch (emAnalise[linha][coluna]) {
                    case 1:
                        aux += (Math.abs(linha - 0) + Math.abs(coluna - 0));
                        break;

                    case 2:
                        aux += (Math.abs(linha - 0) + Math.abs(coluna - 1));
                        break;

                    case 3:
                        aux += (Math.abs(linha - 0) + Math.abs(coluna - 2));
                        break;

                    case 4:
                        aux += (Math.abs(linha - 1) + Math.abs(coluna - 0));
                        break;

                    case 5:
                        aux += (Math.abs(linha - 1) + Math.abs(coluna - 1));
                        break;

                    case 6:
                        aux += (Math.abs(linha - 1) + Math.abs(coluna - 2));
                        break;

                    case 7:
                        aux += (Math.abs(linha - 2) + Math.abs(coluna - 0));
                        break;

                    case 8:
                        aux += (Math.abs(linha - 2) + Math.abs(coluna - 1));
                        break;
                }
            }
        }
        return aux;
    }

}
