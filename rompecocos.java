
public interface rompecocos {

    public long calculo();

    public void buscarCero();

    public void puntoPivote(int linha, int coluna);

    public int getOrigenC();

    public int getOrigenR();

    public rompecocos getOrigen();

    public void imprimir();

    public void setMatriz(int[][] matriz);

    public int[][] getMatriz();

    @Override
    public boolean equals(Object obj);

    public rompecocos clone();

    public boolean finalizar();

}
