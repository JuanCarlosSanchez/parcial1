
import java.util.Arrays;

/**
 * @author gusta
 */
public class rompe4 implements rompecocos {

    private rompe4 pai;
    private long profundidade;
    private int[][] matriz;
    private int zeroColuna;
    private int zeroLinha;
    private manhattan4 heuristica;

    public rompe4(int[][] element, rompe4 pai, long profundidade, int linha, int coluna, manhattan4 heuristica) {
        this.pai = pai;
        this.profundidade = profundidade;
        this.zeroLinha = linha;
        this.zeroColuna = coluna;
        this.matriz = element;
        if (zeroColuna != -1 && zeroLinha != -1 && element[zeroLinha][zeroColuna] != 0) {
            buscarCero();
        }
        this.heuristica = heuristica;
    }


    @Override
    public long calculo() {
        return this.profundidade + heuristica.getH2(matriz);
    }


    @Override
    public void buscarCero() {
        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz.length; coluna++) {
                if (matriz[linha][coluna] == 0) {
                    puntoPivote(linha, coluna);
                }
            }
        }
    }

    @Override
    public rompe4 getOrigen() {
        return pai;
    }

    @Override
    public void puntoPivote(int linha, int coluna) {
        this.zeroLinha = linha;
        this.zeroColuna = coluna;
    }

    @Override
    public int getOrigenC() {
        return zeroColuna;
    }

    @Override
    public int getOrigenR() {
        return zeroLinha;
    }

    @Override
    public void setMatriz(int[][] matriz) {
        this.matriz = matriz;
    }

    @Override
    public void imprimir() {
        for (int linha = 0; linha < matriz.length; linha++) {
            for (int coluna = 0; coluna < matriz.length; coluna++) {
                System.out.print(matriz[linha][coluna] + "  ");
            }
            System.out.println("");
        }
        System.out.println("");
    }

    @Override
    public int[][] getMatriz() {
        return matriz;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final rompe4 other = (rompe4) obj;
        if (this.zeroColuna != other.zeroColuna) {
            return false;
        }
        if (this.zeroLinha != other.zeroLinha) {
            return false;
        }
        if (!Arrays.deepEquals(this.matriz, other.matriz)) {
            return false;
        }
        return true;
    }

    @Override
    public rompe4 clone() {
        int[][] matrizC = new int[4][4];
        matrizC[0][0] = matriz[0][0];
        matrizC[0][1] = matriz[0][1];
        matrizC[0][2] = matriz[0][2];
        matrizC[0][3] = matriz[0][3];

        matrizC[1][0] = matriz[1][0];
        matrizC[1][1] = matriz[1][1];
        matrizC[1][2] = matriz[1][2];
        matrizC[1][3] = matriz[1][3];

        matrizC[2][0] = matriz[2][0];
        matrizC[2][1] = matriz[2][1];
        matrizC[2][2] = matriz[2][2];
        matrizC[2][3] = matriz[2][3];

        matrizC[3][0] = matriz[3][0];
        matrizC[3][1] = matriz[3][1];
        matrizC[3][2] = matriz[3][2];
        matrizC[3][3] = matriz[3][3];

        rompe4 clone = new rompe4(matrizC, this, this.profundidade + 1, getOrigenR(), getOrigenR(), heuristica);
        return clone;
    }

    @Override
    public boolean finalizar() {
        if (matriz[0][0] != 1) {
            return false;
        }
        if (matriz[0][1] != 2) {
            return false;
        }
        if (matriz[0][2] != 3) {
            return false;
        }
        if (matriz[0][3] != 4) {
            return false;
        }
        if (matriz[1][0] != 5) {
            return false;
        }
        if (matriz[1][1] != 6) {
            return false;
        }
        if (matriz[1][2] != 7) {
            return false;
        }
        if (matriz[1][3] != 8) {
            return false;
        }
        if (matriz[2][0] != 9) {
            return false;
        }
        if (matriz[2][1] != 10) {
            return false;
        }
        if (matriz[2][2] != 11) {
            return false;
        }
        if (matriz[2][3] != 12) {
            return false;
        }
        if (matriz[3][0] != 13) {
            return false;
        }
        if (matriz[3][1] != 14) {
            return false;
        }
        if (matriz[3][2] != 15) {
            return false;
        }
        if (matriz[3][3] != 0) {
            return false;
        }
        return true;
    }


}
